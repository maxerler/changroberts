package ca.ubc.changroberts;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * A class implementing the Chang-Roberts leader election
 * algorithm, in which the process with the largest program
 * id is chosen as the leader. Further information on the
 * algorithm can be found at the link below.
 *
 * @author max
 * @see <a href="http://en.wikipedia.org/wiki/Chang_and_Roberts_algorithm">Chang Roberts Algorithm</a>
 *
 */
public class ChangRoberts {
	private static final int MAX_PID = 30000;
	private static final int MIN_PID = 20000;
	private static Random r = new Random();
	private static Object LOCK = new Object();
	private static Set<Integer> usedPids;
	private static List<Process> processes;
	private static int leader = -1;
	
	/**
	 * Main method. Spawns n processes and triggers a leader election
	 * according to the Chang-Roberts algorithm. The processes
	 * are set up in a unidirectional ring such that each process knows
	 * the port number of the next process in the ring for message passing.
	 * Once a leader has been elected, the program exits.
	 *
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {
		if (args.length != 2) {
			System.out.println("Please supply the number of processes to run and the number of elections to hold.");
			System.out.println("Usage: java ChangRoberts <number of processes> <number of elections>");
			System.exit(1);
		}
		
		int numProcs = 0;
		int numElections = 0;
		
		try {
			numProcs = Integer.parseInt(args[0]);
			numElections = Integer.parseInt(args[1]);
		} catch (NumberFormatException e) {
			System.out.println("Please supply the number of processes to run and the number of elections to hold.");
			System.out.println("Usage: java ChangRoberts <number of processes> <number of elections>");
			System.exit(1);
		}
		
		if (numProcs < 3 || numElections < 1) {
			System.out.println("Please run this program with at least 3 processes and 1 election.");
			System.out.println("Usage: java ChangRoberts <number of processes> <number of elections>");
			System.exit(1);
		}
		
		initializeRing(numProcs);
		runElections(numElections);

		System.exit(0);
	}

	/**
	 * Runs numElections iterations of leader election and reports
	 * the results of each election.
	 *
	 * @param numElections
	 * @throws InterruptedException
	 */
	private static void runElections(int numElections) throws InterruptedException {
		for (int i = 0; i < numElections; i++) {
			// initialize leader to -1
			leader = -1;

			// random process starts the election
			processes.get(getRandBetween(0, processes.size() - 1)).startElection();

			// wait for election threads
			awaitCompletion();
			reportLeaders(i);
		}
	}

	/**
	 * Prints the pid and the leader of each process to std out.
	 *
	 * @param iteration
	 */
	private static void reportLeaders(int iteration) {
		System.out.println("Election report for iteration " + iteration + ":");
		for (Process p : processes) {
			int pid = p.getPID();
			if (leader != -1 && leader != p.getLeader()) {
				System.out.println("    WARNING: Process " + pid + " does not agree with a previous process.");
			}
			leader = p.getLeader();
			System.out.println("    Process " + pid + " has leader " + leader);
		}
		System.out.println();
	}

	/**
	 * Initializes the synchronous process ring and starts
	 * all processes in their own threads.
	 *
	 * @param numProcs
	 */
	private static void initializeRing(int numProcs) {
		usedPids = new HashSet<Integer>();
		processes = new ArrayList<Process>();

		int firstPid = getRandPID();
		int pid = firstPid;
		int nextPid = getRandPID();
		
		Process p = new Process(pid, nextPid);
		processes.add(p);
		new Thread(p).start();
		
		for (int i = 0; i < numProcs - 2; i++) {
			pid = nextPid;
			nextPid = getRandPID();
			p = new Process(pid, nextPid);
			processes.add(p);
			new Thread(p).start();
		}
		
		p = new Process(nextPid, firstPid);
		processes.add(p);
		new Thread(p).start();
	}
	
	/**
	 * Produces a random, unique number between MIN_PID and MAX_PID,
	 * inclusive. Repeated calls to this method will never
	 * return the same number, provided the method is called
	 * fewer than (MAX_PID - MIN_PID + 1) times. Calling this method
	 * more than (MAX_PID - MIN_PID + 1) times will result
	 * in an infinite loop.
	 *
	 * @return
	 */
	private static int getRandPID() {
		int pid = getRandBetween(MIN_PID, MAX_PID);
		while (usedPids.contains(pid)) {
			pid = getRandBetween(MIN_PID, MAX_PID);
		}
		usedPids.add(pid);
		return pid;
	}

	/**
	 * Produces a random, unique number between min and max,
	 * inclusive.
	 *
	 * @param min
	 * @param max
	 * @return
	 */
	private static int getRandBetween(int min, int max) {
		return r.nextInt((max - min) + 1) + min;
	}
	
	/**
	 * Blocks the current thread until okToTerminate() is called.
	 * This method is called by the main thread to provide election
	 * threads enough time to complete the leader election.
	 *
	 * @throws InterruptedException
	 */
	private static void awaitCompletion() throws InterruptedException {
		synchronized (LOCK) {
			LOCK.wait();
		}
	}

	/**
	 * Notifies the main thread that the election has completed and
	 * that it is okay to terminate the program. This method should be
	 * called by the process that has been elected as the leader after
	 * the election has finished.
	 */
	public static void electionComplete() {
		synchronized (LOCK) {
			LOCK.notify();
		}
	}
}
