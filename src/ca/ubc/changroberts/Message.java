package ca.ubc.changroberts;

import java.io.Serializable;

/**
 * A message with a type and the pid of the process
 * that sent it.
 *
 * @author max
 *
 */
public class Message implements Serializable {
	
	public static enum MessageType {
		ELECTION,
		ELECTED;
	}

	private static final long serialVersionUID = -7430570626533362431L;
	private MessageType type;
	private int pid;

	public Message(MessageType type, int pid) {
		this.type = type;
		this.pid = pid;
	}
	
	public MessageType getMesageType() {
		return type;
	}
	
	public int getPid() {
		return pid;
	}
}
