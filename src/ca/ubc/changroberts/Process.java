package ca.ubc.changroberts;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import ca.ubc.changroberts.Message.MessageType;

/**
 * A process that participates in the leader election.
 * Capable of sending messages to the next process in
 * the ring.
 *
 * @author max
 *
 */
public class Process implements Runnable {
	
	public static enum Status {
		PARTICIPANT,
		NON_PARTICIPANT;
	}
	
	private int pid;
	private ServerSocket ssoc;
	private int next;
	private Status status;
	private int leader;
	private boolean run;
	
	/**
	 * Constructor. Initializes a process to a non-participating state
	 * with an illegal leader process. Also opens a server socket with
	 * port address pid.
	 *
	 * @param pid
	 * @param next
	 */
	public Process(int pid, int next) {
		this.pid = pid;
		this.next = next;
		this.status = Status.NON_PARTICIPANT;
		this.leader = -1;
		this.run = true;
		
		try {
			this.ssoc = new ServerSocket(pid);
		} catch (IOException e) {
			System.out.println("Error opening socket for process " + pid + ": " + e.getMessage());
			System.out.println("Terminating.");
			System.exit(1);
		}
	}
	
	/**
	 * Sends a message to the next process in the ring. This
	 * message can be either an election message or an elected
	 * message.
	 *
	 * @param m
	 */
	private void sendMessage(Message m) {
		try {
			Socket nextSoc = new Socket("localhost", next);
			ObjectOutputStream oos = new ObjectOutputStream(nextSoc.getOutputStream());
			oos.writeObject(m);
			oos.close();
			nextSoc.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Receives a message from the previous process in the ring.
	 * This method will block until a message is ready.
	 *
	 * @return
	 */
	private Message getMessage() {
		try {
			Socket soc = ssoc.accept();
			ObjectInputStream ois = new ObjectInputStream(soc.getInputStream());
			Message m = (Message) ois.readObject();
			ois.close();
			soc.close();
			return m;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		} 
	}

	/**
	 * Handles the message m. This is where the bulk of the Chang-Roberts
	 * algorithm is implemented.
	 *
	 * If the message is an elected message, we
	 * forward the message around the ring until it returns to the original
	 * (leader) process.
	 *
	 * If it is an election message, and the pid of the
	 * sending process is greater than our own, we forward the message along.
	 * If the pid of the message is our own pid, then we proclaim ourselves
	 * the leader and send an elected message along. If the pid of the message
	 * is less than our own and we haven't already sent a message, then we
	 * switch our status to participant and send our own election message instead.
	 * If we have already sent an election message, then we simply ignore the lesser
	 * pid.
	 *
	 * @param m
	 */
	private void handleMessage(Message m) {
		switch (m.getMesageType()) {
		case ELECTED:
			if (m.getPid() == pid) {
				//System.out.println("We are the leader with pid: " + pid); // TODO
				// let the main thread know it's okay to exit
				ChangRoberts.electionComplete();
			} else {
				leader = m.getPid();
				status = Status.NON_PARTICIPANT;
				//System.out.println("Leader elected with pid: " + leader); // TODO
				sendMessage(m);
			}
			//shutDown(); // TODO
			break;
		case ELECTION:
			if (m.getPid() > pid) {
				// send it along
				status = Status.PARTICIPANT;
				sendMessage(m);
			} else if (m.getPid() == pid) {
				// we are the leader
				leader = pid;
				status = Status.NON_PARTICIPANT;
				sendMessage(new Message(MessageType.ELECTED, pid));
			} else if (Status.NON_PARTICIPANT.equals(status)) {
				// send our pid instead
				status = Status.PARTICIPANT;
				sendMessage(new Message(MessageType.ELECTION, pid));
			}
			break;
		}
	}
	
	/**
	 * Shuts down the current process. Closes the server socket for
	 * this process and sets the run flag to false, allowing the
	 * run() method to return and the thread to terminate.
	 */
	private void shutDown() {
		Thread.yield(); // let a process we sent a message to run
		try {
			ssoc.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		run = false;
	}
	
	/**
	 * Starts an election. The current process marks itself as a participant
	 * and sends an election message to the next process in the ring with its
	 * own pid.
	 */
	public void startElection() {
		status = Status.PARTICIPANT;
		sendMessage(new Message(MessageType.ELECTION, pid));
	}
	
	/**
	 * Returns the pid of this process.
	 *
	 * @return
	 */
	public int getPID() {
		return pid;
	}

	/**
	 * Returns the pid of the leader according to this process.
	 *
	 * @return
	 */
	public int getLeader() {
		return leader;
	}

	/**
	 * Run method. Allows this class to run in a separate thread. This
	 * method loops, receiving messages and handling them appropriately.
	 * When the election has completed, run is set to false and this method
	 * is allowed to return.
	 */
	@Override
	public void run() {
		while (run) {
			Message m = getMessage();
			if (m != null) {
				handleMessage(m);
			}
		}
	}
}
